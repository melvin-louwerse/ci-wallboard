import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {SettingsComponent} from './Component/Setting/settings.component';
import {ProjectSettingsComponent} from './Component/Setting/Project/projectsettings.component';
import {UserSettingsComponent} from './Component/Setting/User/usersettings.component';
import {ReposComponent} from './Component/Repos/repos.component';
import {ProjectComponent} from './Component/Project/project.component';

const routes: Routes = [
    {path: 'repos', component: ReposComponent},
    {path: 'settings', component: SettingsComponent},
    {path: 'project-settings', component: ProjectSettingsComponent},
    {path: 'user-settings', component: UserSettingsComponent},
    {path: 'project/:repo/:project', component: ProjectComponent},
    {path: 'project/:repo/:project/:private', component: ProjectComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}