import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {MenuComponent} from "./Component/Menu/menu.component";
import {Store} from "./Service/store";
import {CookieService} from "./Service/cookie";
import {CiRequestFactory} from "./Service/ciRequestFactory";
import {Settings} from "./Service/settings";

import {AppRoutingModule} from "./app-routing.module";
import {StateClass} from "./Pipes/state.class";
import {Gravatar} from "./Pipes/gravatar";
import {TimeAgo} from "./Pipes/timeago";
import {Wallboard} from "./Component/Wallboard/wallboard.component";
import {ReposComponent} from "./Component/Repos/repos.component";
import {SettingsComponent} from "./Component/Setting/settings.component";
import {ProjectSettingsComponent} from "./Component/Setting/Project/projectsettings.component";
import {UserSettingsComponent} from "./Component/Setting/User/usersettings.component";
import {ProjectComponent} from "./Component/Project/project.component";
import {StatusFailedComponent} from "./Component/Status/status.failed.component";
import {StatusSuccessComponent} from "./Component/Status/status.success.component";
import {StatusPendingComponent} from "./Component/Status/status.pending.component";
import {ArraySortPipe} from "./Pipes/arrayfilter";
import {ReposService} from "./Component/Repos/repos.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {AppComponent} from "./app.component";
import {TrimPipe} from "./Pipes/trim";

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToasterModule
  ],
  declarations: [
    SettingsComponent,
    ProjectSettingsComponent,
    UserSettingsComponent,
    ProjectComponent,
    MenuComponent,
    StatusSuccessComponent,
    StatusPendingComponent,
    StatusFailedComponent,
    AppComponent,
    StateClass,
    Gravatar,
    TimeAgo,
    ArraySortPipe,
    ReposComponent,
    Wallboard,
    TrimPipe
  ],
  providers: [
    CiRequestFactory,
    Settings,
    Store,
    CookieService,
    ReposService,
  ],
  bootstrap: [MenuComponent, Wallboard]
})
export class AppModule {
}
