import {Injectable} from '@angular/core';
import {RepoSettings} from '../Component/Setting/reposettings.model'
import {Store} from './store';

@Injectable()

export class Settings {
    private useMocks: boolean = false;
    private privateUri: string = 'https://api.travis-ci.com/';
    private openSourceUri: string = 'https://api.travis-ci.org/';
    private users: any = [];
    private projects: any = {};
    //holds what repos use what connection data.
    private repos: {} = {};
    private slug: string = "";
    private acceptHeader: string = 'application/json';
    private store: Store;

    constructor(store: Store) {
        this.store = store;
        this.users = this.store.getValue('users');

    }

    setUsers(users): void {
        this.users = users;
        this.store.setValue('user-settings', users);
    }

    getUsers() {
        return this.users;
    }

    setProjects(projects) {
        this.projects = projects;
        this.store.setValue('projectData', JSON.stringify(projects));
    }

    getToken(isPrivate, token) {
        if (isPrivate == true) {
            return 'token ' + token;
        } else {
            return null;
        }
    }

    getAcceptHeader() {
        return this.acceptHeader;
    }

    getSlug() {
        return this.slug;
    }

    getUri(repoSettings: RepoSettings) {
        if (this.useMocks) {
            if (repoSettings.isPrivate()) {
                return 'http://localhost:9002/mock.php?type=private&path=';
            } else {
                return 'http://localhost:9002/mock.php?type=protected&path=';
            }
        } else if (repoSettings.isPrivate()) {
            return this.privateUri;
        } else {
            return this.openSourceUri;
        }
    }

    loadProjectData() {
        // data.projects = JSON.parse(TW.helpers.getCookie('projectData'));
        // if (data.projects == null) {
        //     data.projects = {};
        // }
    }
}

