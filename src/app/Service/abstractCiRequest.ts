import {Settings} from "./settings";
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {RepoSettings} from "../Component/Setting/reposettings.model";

export abstract class AbstractCiRequest {
    protected http: Http;
    protected settings: Settings;

    constructor(http: Http, settings: Settings) {
        this.http = http;
        this.settings = settings;
    }

    abstract getBuild(settings: RepoSettings, slug: String, id: number, buildId);

    /**
     * @param {RepoSettings} settings
     * @param {string} slug
     * @param {number} id
     * @param {boolean} all
     * @returns {Promise<any>}
     */
    abstract getBuilds(settings: RepoSettings, slug: String, id: number, all: boolean);

    /**
     * @param {RepoSettings} settings
     * @returns {Promise<any>}
     */
    abstract loadProjectData(settings: RepoSettings);
}
