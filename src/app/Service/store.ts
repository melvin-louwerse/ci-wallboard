'use strict';

import {Injectable} from '@angular/core';
import {RepoSettings} from '../Component/Setting/reposettings.model';
import {CookieService} from './cookie';

@Injectable()

export class Store {
  useLocalStorage: boolean = false;
  private _cookieService: CookieService;

  constructor(cookieService: CookieService) {
    this._cookieService = cookieService;
    this.useLocalStorage = Store.hasLocalStorage();
  }

  /**
   * Gets a cookie for the given name.
   *
   * @param name
   * @returns {*}
   */
  getCookie(name): string {
    let value = this._cookieService.get(name);
    if (typeof value === 'undefined') {
      return null;
    }
    return JSON.parse(value);
  }

  /**
   * Sets a cookie to the given name.
   *
   * @param {string} name
   * @param {string} value
   * @param {number} days
   */
  setCookie(name: string, value: string, days: number): void {
    if (typeof(value) !== "string") {
      value = JSON.stringify(value);
    }

    this._cookieService.put(name, value, '/', new Date('2030-12-12'));
  }

  setToLocalStorage(name, value): void {
    if (typeof(value) !== "string") {
      value = JSON.stringify(value);
    }
    window.localStorage.setItem(name, value);
  }

  getFromLocalStorage(name): any {
    let data = window.localStorage.getItem(name);


    try {
      return JSON.parse(data);
    }
    catch (e) {
      return data;
    }
  }

  getValue(name: string): any {
    if (Store.hasLocalStorage()) {
      return this.getFromLocalStorage(name);
    } else {
      return this.getCookie(name);
    }
  }

  setValue(name, value): void {
    if (Store.hasLocalStorage()) {
      return this.setToLocalStorage(name, value);
    } else {
      return this.setCookie(name, value, 700);
    }
  }

  static hasLocalStorage(): boolean {
    return typeof(Storage) !== "undefined";
  }


  /**
   * @returns {Array<RepoSettings>}`
   */
  getRepoSettings(): Array<RepoSettings> {
    let users = this.getValue('users');
    let repos = [];
    if (typeof users !== 'undefined' && users !== null) {
      repos = users.map((settings) => {
        return new RepoSettings(settings.name, settings.token, settings.refreshRate, settings.environment, '?????');
      });
    }

    return repos;
  }

  /**
   * @returns {RepoSettings}
   */
  getRepoSetting(repoName: string, isPrivate: boolean): RepoSettings {
    let users = this.getValue('users');

    let repoSettings: RepoSettings = null;
    users.forEach((settings) => {
      if (settings.name === repoName && (isPrivate === false || settings.token !== "")) {
        repoSettings = new RepoSettings(settings.name, settings.token, settings.refreshRate, settings.environment, '?????');
      }
    });

    return repoSettings;
  }
}
