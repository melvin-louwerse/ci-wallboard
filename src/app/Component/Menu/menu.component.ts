import {Component} from '@angular/core';
import {CiRequestFactory} from "../../Service/ciRequestFactory";
import {RepoSettings} from '../Setting/reposettings.model';
import {Store} from "../../Service/store";
import {Project} from '../Project/project.model';

@Component({
    selector: 'top-menu',
    templateUrl: './menu.component.html',
})

export class MenuComponent {
    private store: Store;
    private requestFactory: CiRequestFactory;
    private repoSettings: Array<RepoSettings>;
    private users: any;
    public projects: any = [];

    constructor(store: Store, requestFactory: CiRequestFactory) {
        this.store = store;
        this.requestFactory = requestFactory;
        this.repoSettings = this.store.getRepoSettings();
        this.loadUsers();
        this.getProjects();
    }

    /**
     * Load users from the store and set them to the users variable.
     */
    loadUsers() {
        this.users = this.store.getValue('user-settings');
    }

    getProjects() {
        this.repoSettings.forEach((repoSetting) => {
            if (!this.isEmpty(repoSetting.getName())) {
                console.info('MENU COMPONENT: adding menu item ' + repoSetting.getKey());
                let request = this.requestFactory.create(repoSetting.getEnvironment());
                let resource = request.loadProjectData(repoSetting);
                resource.then(
                    projects => {
                        this.addToMenu(projects, repoSetting)
                    },
                    error => {
                        console.error(error);
                    },
                )
            }
        }, this);
    }

    isEmpty(val) {
        return (val === "" || typeof(val) === "undefined" || val === null);
    }

    addToMenu(projects: Array<Project>, repoSetting: RepoSettings) {
        projects.forEach((project) => {
            let path = project.getSlug();
            if (repoSetting.isPrivate()) {
                path += '/private';
            }
            this.projects.push({slug: project.getSlug(), path: path});
        }, this);
    }
}
