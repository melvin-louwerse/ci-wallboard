'use strict';
import {Commit} from './commit.model';

export class GitlabBuild {
  private _id;
  private _number;
  private _name;
  private _state;
  private _startedAt;
  private _finishedAt;
  private _isPr;
  private _commit;
  private _type;
  private _prnr;
  private _displayName: string;
  private _manualAt: number;

  constructor(id: number, number: number, name: string, state: string, startedAt: number, finishedAt: number, isPr: boolean, prnr: number, type: string, commit: Commit) {
    this._id = id;
    this._number = number;
    this._name = name;
    let parts = name.split('/');
    this._displayName = parts[parts.length -1];
    this._state = state;
    this._startedAt = startedAt;
    this._finishedAt = finishedAt;
    // When waiting it has no finished date but we want to show it on teh correct position in the list so return
    // the created date as that is closest to the in manual date.
    this._manualAt = this._startedAt;
    this._isPr = isPr;
    this._prnr = prnr;
    this._type = type;
    this._commit = commit;
  }

  getId(): any {
    return this._id;
  }

  getType(): any {
    return this._type;
  }

  getNumber() {
    return this._number;
  }

  getName() {
    return this._displayName;
  }

  getState() {
    return this._state;
  }

  getStartedAt() {
    return this._startedAt;
  }

  getFinishedAt() {
    if (this._state === 'manual') {
      return this._manualAt;
    }
    return this._finishedAt;
  }

  isPr() {
    return this._isPr;
  }

  getPrnr() {
    return this._prnr;
  }

  /**
   *
   * @returns {Commit}
   */
  getCommit() {
    return this._commit;
  }

  isBuilding() {
    return this._state === 'running';
  }

  isPassing() {
    return this._state === 'success' || this._state === 'canceled' || this._state === 'manual';
  }

  isFailed() {
    return this._state === 'failed';
  }

  hasRecentError() {
    let dt = new Date(Date.parse(this._finishedAt));
    let now = new Date();

    let diff = now.getTime() - dt.getTime();

    let minutes = Math.floor((diff / (60000)));

    if (minutes < 5) {
      return true;
    }

    return false;
  }
}
