'use strict';

export class Commit {
    private _id: number;
    private _branch: string;
    private _committerEmail: string;
    private _committerName: string;
    private _message: string;
    private _isPullRequest: boolean;
    private _avatarUrl: string;

    constructor(id: number, branch: string, committerEmail: string, committerName: string, message: string, pullRequestNr: string, avatarUrl : string = null) {
        this._id = id;
        this._branch = branch;
        this._committerEmail = committerEmail;
        this._committerName = committerName;
        this._message = message;
        this._avatarUrl = avatarUrl;
        this._isPullRequest = pullRequestNr !== null;
    }

    getAvatarUrl(): string {
        return this._avatarUrl;
    }
    getId(): number {
        return this._id;
    }

    getBranch(): string {
        return this._branch;
    }

    getCommitterEmail(): string {
        return this._committerEmail;
    }

    getCommitterName(): string {
        return this._committerName;
    }

    getMessage(): string {
        return this._message;
    }

    getIsPullRequest(): boolean {
        return this._isPullRequest;
    }
}
