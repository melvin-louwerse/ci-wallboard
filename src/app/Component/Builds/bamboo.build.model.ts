'use strict';
import {Commit} from './commit.model';

export class BambooBuild {
    private _id;
    private _number;
    private _name;
    private _state;
    private _startedAt;
    private _finishedAt;
    private _isPr;
    private _commit;
    private _type;
    private _prnr;

    constructor(
      id: number, number: number, name: string, state: string, startedAt: number, finishedAt: number, isPr: boolean, prnr: number, type: string, commit: Commit) {
        this._id = id;
        this._number = number;
        this._name = name;
        this._state = state;
        this._startedAt = startedAt;
        this._finishedAt = finishedAt;
        this._isPr = isPr;
        this._prnr = prnr;
        this._type = type;
        this._commit = commit;
    }

    getId(): any {
    return this._id;
}

    getType(): any {
    return this._type;
}

    getNumber() {
        return this._number;
    }

    getName() {
        return this._name;
    }

    getState() {
        return this._state;
    }

    getStartedAt() {
        return this._startedAt;
    }

    getFinishedAt() {
        return this._finishedAt;
    }

    isPr() {
        return this._isPr;
    }

    getPrnr() {
        return this._prnr;
    }

    /**
     *
     * @returns {Commit}
     */
    getCommit() {
        return this._commit;
    }

    isBuilding() {
        //Seems bamboo has no building state
        return false;
    }

    isPassing() {
        return this._state === 'Successful';
    }

    isFailed() {
        return this._state === 'Failed';
    }

    hasRecentError() {
        let dt = new Date(Date.parse(this._finishedAt));
        let now = new Date();

        let diff = now.getTime() - dt.getTime();

        let minutes = Math.floor((diff / (60000)));

        if (minutes < 5) {
            return true;
        }

        return false;
    }
}
